'use strict';

var utils = require('../utils/writer.js');
var Family = require('../service/FamilyService');

module.exports.addFamily = function addFamily (req, res, next, body) {
  Family.addFamily(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteFamilyById = function deleteFamilyById (req, res, next, familyID) {
  Family.deleteFamilyById(familyID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.familyFamilyIDGET = function familyFamilyIDGET (req, res, next, familyID) {
  Family.familyFamilyIDGET(familyID)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.searchFamily = function searchFamily (req, res, next, searchString, skip, limit) {
  Family.searchFamily(searchString, skip, limit)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
