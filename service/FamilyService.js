'use strict';
const loki = require('lokijs');

var db = new loki('sandbox.db', {
  autoload: true,
  autoloadCallback : databaseInitialize,
  autosave: true,
  autosaveInterval: 4000
});
function databaseInitialize() {
  var log = db.getCollection("items");

  if (log === null) {
    db.addCollection("items");
  }

}
/**
 * adds an family
 * Adds an family to the system
 *
 * body Family Family to add (optional)
 * no response value expected for this operation
 **/
exports.addFamily = function(body) {
  var items = db.getCollection('items');
  items.insert(body)
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete family by ID
 * delete
 *
 * familyID UUID uuid of family
 * no response value expected for this operation
 **/
exports.deleteFamilyById = function(familyID) {
  var items = db.getCollection('items');
  items.removeWhere({'id': familyID})

  return new Promise(function(resolve, reject) {
    resolve();
  });
}




/**
 * Get a family by ID
 *
 * familyID UUID uuid of family
 * returns Family
 **/
exports.familyFamilyIDGET = function(familyID) {
  var items = db.getCollection('items');
  return new Promise(function(resolve, reject) {
    resolve(items.findOne({'id':familyID}));
  });

}


/**
 * searches family
 * By passing in the appropriate options, you can search for available family in the system 
 *
 * searchString String pass an optional search string for looking up family (optional)
 * skip Integer number of records to skip for pagination (optional)
 * limit Integer maximum number of records to return (optional)
 * returns List
 **/
exports.searchFamily = function(searchString,skip,limit) {
  var items = db.getCollection('items');
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ ].concat(items.data);

    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


exports.delFamilyById = function(familyId) {
  var items = db.getCollection('items');
  items.findOne({'id': lawOrderId}).remove();

  return new Promise(function(resolve, reject) {
    resolve();
  });
}